function [B, allProps, allSimulationTimes] = getATSystemAndSpecs()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
% T = 20
allProps{1} = STL_Formula('Problem1_T20', 'ev_[0,20](RPM[t] >= 2000)');
allSimulationTimes(1) = 40;

% T = 30
allProps{2} = STL_Formula('Problem1_T30', 'ev_[0,30](RPM[t] >= 2000)');
allSimulationTimes(2) = 40;

% T = 40
allProps{3} = STL_Formula('Problem1_T40', 'ev_[0,40](RPM[t] >= 2000)');
allSimulationTimes(3) = 40;

%%%%%%%%%
% phi_2 %
%%%%%%%%%
allProps{4} = STL_Formula('Problem2', 'alw_[0, 30](ev_[0,10](RPM[t] <= 3500 or RPM[t] >= 4500))');
allSimulationTimes(4) = 40;

%%%%%%%%%
% phi_3 %
%%%%%%%%%
% T = 4.5
allProps{5} = STL_Formula('Problem3_T4_5', 'alw_[0,4.5](not(gear[t]==4))');
allSimulationTimes(5) = 10;

% T = 5
allProps{6} = STL_Formula('Problem3_T5', 'alw_[0,5](not(gear[t]==4))');
allSimulationTimes(6) = 10;

%%%%%%%%%
% phi_4 %
%%%%%%%%%
% T = 1
allProps{7} = STL_Formula('Problem4_T1', 'ev_[0,39](alw_[0,1](gear[t]==3))');
allSimulationTimes(7) = 40;

% T = 2
allProps{8} = STL_Formula('Problem4_T2', 'ev_[0,38](alw_[0,2](gear[t]==3))');
allSimulationTimes(8) = 40;

%%%%%%%%%
% phi_5 %
%%%%%%%%%
% T = 1
string1 = 'alw_[0,38.96]((not(gear[t]==1) and ev_[0,0.04](gear[t]==1)) => (alw_[0.04,1.04](gear[t]==1)))';
string2 = 'alw_[0,38.96]((not(gear[t]==2) and ev_[0,0.04](gear[t]==2)) => (alw_[0.04,1.04](gear[t]==2)))';
string3 = 'alw_[0,38.96]((not(gear[t]==3) and ev_[0,0.04](gear[t]==3)) => (alw_[0.04,1.04](gear[t]==3)))';
string4 = 'alw_[0,38.96]((not(gear[t]==4) and ev_[0,0.04](gear[t]==4)) => (alw_[0.04,1.04](gear[t]==4)))';
allProps{9} = STL_Formula('Problem5_T1', ['(' string1 ' and ' string2 ') and (' string3 ' and ' string4 ')']);
allSimulationTimes(9) = 40;

% T = 2
string1 = 'alw_[0,37.96]((not(gear[t]==1) and ev_[0,0.04](gear[t]==1)) => (alw_[0.04,2.04](gear[t]==1)))';
string2 = 'alw_[0,37.96]((not(gear[t]==2) and ev_[0,0.04](gear[t]==2)) => (alw_[0.04,2.04](gear[t]==2)))';
string3 = 'alw_[0,37.96]((not(gear[t]==3) and ev_[0,0.04](gear[t]==3)) => (alw_[0.04,2.04](gear[t]==3)))';
string4 = 'alw_[0,37.96]((not(gear[t]==4) and ev_[0,0.04](gear[t]==4)) => (alw_[0.04,2.04](gear[t]==4)))';
allProps{10} = STL_Formula('Problem5_T2', ['(' string1 ' and ' string2 ') and (' string3 ' and ' string4 ')']);
allSimulationTimes(10) = 40;

%%%%%%%%%
% phi_6 %
%%%%%%%%%
% T = 10
allProps{11} = STL_Formula('Problem6_T10', 'alw_[0,10](speed[t] <= 85) or ev_[0,20](RPM[t] >= 4500)');
allSimulationTimes(11) = 20;

% T = 12
allProps{12} = STL_Formula('Problem6_T12', 'alw_[0,12](speed[t] <= 85) or ev_[0,20](RPM[t] >= 4500)');
allSimulationTimes(12) = 20;

%%%%%%%%%
% phi_7 %
%%%%%%%%%
prereq1 = 'alw_[0,1](gear[t] == 1)';
prereq2 = 'alw_[2,4](gear[t] == 2)';
prereq3 = 'alw_[5,7](gear[t] == 3)';
prereq4 = 'alw_[8,10](gear[t] == 3)';
prereq5 = 'alw_[12,15](gear[t] == 2)';
allPrereqs = ['((((' prereq1 ' and ' prereq2 ') and ' prereq3 ') and ' prereq4 ') and ' prereq5 ')'];
allProps{13} = STL_Formula('Problem12', ['not(' allPrereqs ')']);
allSimulationTimes(13) = 20;

%%%%%%%%%
% phi_8 %
%%%%%%%%%
% omegabar = 3000
prereq1 = 'gear[t] == 4';
prereq2 = 'In1[t] < 50';
prereq3 = 'In1[t] > 45';
allPrereqs = ['((' prereq1 ' and ' prereq2 ') and ' prereq3 ')'];
allProps{14} = STL_Formula('Problem12', ['alw_[0,20](' allPrereqs ' => RPM[t] < 3000)']);
allSimulationTimes(14) = 20;

% omegabar = 3500
prereq1 = 'gear[t] == 4';
prereq2 = 'In1[t] < 50';
prereq3 = 'In1[t] > 45';
allPrereqs = ['((' prereq1 ' and ' prereq2 ') and ' prereq3 ')'];
allProps{15} = STL_Formula('Problem12', ['alw_[0,20](' allPrereqs ' => RPM[t] < 3500)']);
allSimulationTimes(15) = 20;


% Create the BreachSimulinkSystem object
B = BreachSimulinkSystem('autotrans_mod04');

% Number of control points: 7 for the accelerator, 3 for the brake
nCP = [7 3];

% Create the input generator according to standard Breach syntax
input_gen.type = 'UniStep';   % uniform time steps
input_gen.cp = nCP;             % number of control points
input_gen.method = {'pchip', 'pchip'};

% Set the input generator of the BreachSimulinkSystem object
B.SetInputGen(input_gen);

% Set range to [0 100] for all accelerator parameters (In1)
for k = 1:nCP(1)
    % Set range of accelerator to [0 100]
    eval(['B.SetParamRanges({''In1_u' num2str(k-1) '''}, [0 100]);']);
end

% Set range to [0 500] for all brake parameters (In2)
for k = 1:nCP(2)
    % Set range of brake to [0 500]
    eval(['B.SetParamRanges({''In2_u' num2str(k-1) '''}, [0 500]);']);
end

end

