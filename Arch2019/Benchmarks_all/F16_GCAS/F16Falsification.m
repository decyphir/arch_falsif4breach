function F16Falsification(nFalsifications, maxEval, solversToUse, objFunctionsToUse, F16_propsToUse)
% AUTOTRANSFALSIFICATION Run falsification for F16_GCAS benchmark
%   This function is used to run falsification for the automatic transmission
%   benchmark. There is the possibility to set the number of falsifications
%   to perform for each solver, parameter value, and objective function. In
%   addition, you can change which solvers to use, and which objective
%   functions to evaluate. 
%
%   nFalsifications is the number of falsifications to run for each solver,
%       parameter, and robust semantics. In the paper, nFalsifications is 
%       set to 20. 
%   maxEval is the maximum number of simulations for each falsification. In
%       the paper, maxEval is set to 1000. 
%   solversToUse is a cell array with strings, each one specifying a
%       specific optimization solver to use. In the paper, solversToUse is
%       {'simulated_annealing', 'snobfit', 'cmaes', 'global_nelder_mead'}
%   objFunctionsToUse is a cell array with strings, each one specifying a
%       specific robust semantics to use. In the paper, objFunctionsToUse
%       is {'standard', 'vbool', 'constant'}. Note that:
%           'standard' here is 'Max' in the paper;
%           'vbool' here is 'additive' in the paper; and
%           'constant' here is 'constant' in the paper. 

% Get the BreachSimulinkSystem object, a cell array with the specifications
% to falsify, and a vector with the simulation end time for each
% specification. 
[mdlToUse, propsToUse, allSimulationTimes] = getATSystemAndSpecs();

% We store the results in a folder called 'mat'. If it is not yet created,
% create it here. 
if ~isdir('mat')
    mkdir('mat');
end

% Set the maximum time of each falsification to something really high, to
% prevent it from stopping by time alone. 
maxTime = 10000;

% We store the results in a file with a unique ID based on the current
% time. 
uniqueID = datestr(now, 'yyyy_mm_dd_HHMM');
filename = ['mat/ATResults_' uniqueID '.mat'];

% We save dummy variable once - this is so that the file exists, otherwise
% we can't use save with -append later. 
save(filename, 'nFalsifications');

% Here comes the big loop where we perform each falsification
% The outermost loop is over all solvers
for nSolver = 1:numel(solversToUse)
    thisSolver = solversToUse{nSolver};
    
    % Display solver info (using char(10) as that works for older MATLAB
    % versions like 2013b). 
    disp([char(10) char(10) '*************** SOLVER: ' thisSolver ' ***************' char(10) char(10)]); %#ok<*CHARTEN>
    
    % Next, for each solver we loop over the indices specified in
    % F16_propsToUse
    for nProp = F16_propsToUse
		% Initialize a struct calles allResults, which will store results
		% for this specific solver and specification. 
		allResults = struct();
		
        thisProp = propsToUse{nProp};
        
        % Set zero threshold of "=="-predicates to 0. 
        thisProp = set_params(thisProp,'zero_threshold__',0); % To correspond to Simulink semantics of "==" blocks
        
        % Set the simulation end time of the BreachSimulinkSystem object to
        % the time specified in allSimulationTimes
        mdlToUse.SetTime(0:.01:allSimulationTimes(nProp));
        
        % For each spec, we should perform a number of falsifications that
        % is equal to nFalsifications. 
        for nFalsificationRun = 1:nFalsifications
            
            % Each falsification is performed for four different objective
            % functions. 
            for nObjFunction = 1:numel(objFunctionsToUse)
                
                thisObjFunction = objFunctionsToUse{nObjFunction};
                
                set_semantics(thisProp, thisObjFunction);
                
                disp(['*********** STARTING AT --- solver ' num2str(nSolver) '/' num2str(numel(solversToUse)) ...
                    ' --- prop ' num2str(nProp) '/' num2str(numel(propsToUse)) ' --- run ' ...
                    num2str(nFalsificationRun) '/' num2str(nFalsifications) ...
                    ' --- objFunction ' num2str(nObjFunction) '/' num2str(numel(objFunctionsToUse))]);
                disp(['Current time: ' datestr(now, 'yyyy_mm_dd_HHMM')]);
                
                % Create the FalsificationProblem, set parameters equal to
                % the ones specified earlier
                falsif_pb = FalsificationProblem(mdlToUse, thisProp);
                falsif_pb.max_time = maxTime;
                falsif_pb.max_obj_eval = maxEval;
                falsif_pb.setup_solver(thisSolver);
                
                % Show update once every 1001 iterations - this basically
                % is used to suppress output. 
                falsif_pb.freq_update = 1001;
                
                if nObjFunction == 1
                    % For the first objective function, we store the
                    % start sample that is randomly generated
                    [~, ~, startSample] = falsif_pb.solve();
                else
                    % For all the following objective functions, we use the
                    % same start sample as the first objective function
                    % used.
                    falsif_pb.solve(startSample);
                end
                
                % Store all information we need to create all the tables in
                % the paper, and also store some extra information that can
                % be used for further analysis, in case someone is
                % interested in that!
                allResults(nFalsificationRun, nObjFunction).robustness = falsif_pb.obj_log;
                allResults(nFalsificationRun, nObjFunction).nSimulations = falsif_pb.nb_obj_eval;
                allResults(nFalsificationRun, nObjFunction).timeSpent = falsif_pb.time_spent;
                allResults(nFalsificationRun, nObjFunction).prop = thisProp;
                allResults(nFalsificationRun, nObjFunction).startSample = startSample;
                allResults(nFalsificationRun, nObjFunction).objFunction = thisObjFunction;
                allResults(nFalsificationRun, nObjFunction).solver = thisSolver;
                
            end % End loop over objective functions

        end % End loop over falsifications
        
        % The property has now finished
        % We save the allResults variable in a name that is specific to
        % this solver and specific, namely in the format
        % 'autotrans_solver_propXX' where XX is the specification number. 
		resultsVarName = ['F16_' thisSolver '_prop' num2str(nProp)];
		eval([resultsVarName ' = allResults;']);
		
        % Append the variable to the file where we save all data.
        save(filename, resultsVarName, '-append');
		
		% Clear the variables to save memory
        clear('allResults', resultsVarName);
        
    end % End loop over specifications
    
end % End loop over solvers

end