function [B, allProps, allSimulationTimes] = getWindTurbineSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
% T = 20
allProps{1} = STL_Formula('Problem1', 'alw_[30, 630] BladePitchAngle[t] < 14.2');
allSimulationTimes(1) = 630;


%%%%%%%%%
% phi_2 %
%%%%%%%%%
allProps{2} = STL_Formula('Problem2', 'alw_[30, 630] Torque[t] > 21000 ');
allSimulationTimes(2) = 630;

%%%%%%%%%
% phi_3 %
%%%%%%%%%
allProps{3} = STL_Formula('Problem3', 'alw_[30, 630] Omega[t] <= 14.3');
allSimulationTimes(3) = 630;


%%%%%%%%%
% phi_4 %
%%%%%%%%%
allProps{4} = STL_Formula('Problem4', 'alw_[30, 630](ev_[0,5] abs(BladePitchAngle[t] - Theta_d[t]) <= 1.6)');
allSimulationTimes(4) = 630;

% Create the BreachSimulinkSystem object
init_SimpleWindTurbine;
B = BreachSimulinkSystem('SimpleWindTurbine');
IGcp_wind = BreachSignalGen(wind_cp_signal_gen());
B.InitFn ='init_SimpleWindTurbine';
B.SetInputGen(IGcp_wind);
B.InputGenerator.SetDomain('v0', [8 16]);
Inputs = B.GetInputParamList();
B.SetParamRanges(Inputs, [8. 16.]);

end

