function [B, allProps, allSimulationTimes] = getPowertrainSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialize variables
allProps = {};
allSimulationTimes = [];

% From init_powertrain
assignin('base', 'simTime', 50);
assignin('base', 'measureTime', 1);
assignin('base', 'fault_time', 60);
assignin('base', 'spec_num', 1);
assignin('base', 'fuel_inj_tol', 1);
assignin('base', 'MAF_sensor_tol', 1);
assignin('base', 'AF_sensor_tol', 1);

%%%%%%%%%
% phi_1 %
%%%%%%%%%
% bet = 0.008
prereq1 = '(throttle[t] <  8.8) and (ev_[0, 0.05] (throttle[t] > 40.0))';
prereq2 = '(throttle[t] > 40.0) and (ev_[0, 0.05] (throttle[t] <  8.8))';
allProps{1} = STL_Formula('Problem1', ['alw_[11,50]((' prereq1 ' or ' prereq2 ') =>  (alw_[1,5] (abs(mu[t]) < 0.008)))']);
allSimulationTimes(1) = 50;

%%%%%%%%%
% phi_2 %
%%%%%%%%%
% gam = 0.007
allProps{2} = STL_Formula('Problem2', ['alw_[11, 50] (abs(mu[t]) < 0.007)']);
allSimulationTimes(2) = 50;


% Create the BreachSimulinkSystem object
init_powertrain;
B = BreachSimulinkSystem('AbstractFuelControl_M1');

% Create the input generator according to standard Breach syntax
% Create the BreachSimulinkSystem object
% Number of control points: 7 for the accelerator, 3 for the brake
sig_gen = fixed_cp_signal_gen({'throttle', 'engine'}, [10 1], 'previous'); 

% Set the input generator of the BreachSimulinkSystem object
B.SetInputGen(sig_gen);

% Set the input generator of the BreachSimulinkSystem object

for i = 0:9
    B.SetParamRanges(['throttle_u' num2str(i)], [0 61.2]);
end
B.SetParamRanges('engine_u0', [900 1100]);

end

