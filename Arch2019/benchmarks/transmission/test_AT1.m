%% Interface
%clc;
%clear all;
init_transmission;
B0 = BreachSimulinkSystem('Autotrans_shift');
BO.InitFn = 'init_transmission';

%% Input configuration 1
sig_gen = var_cp_signal_gen({'throttle', 'brake'}, [3 2], 'previous'); 
B1 = B0.copy(); 
B1.SetInputGen(sig_gen);
for i = 0:2
    B1.SetParamRanges(['throttle_u' num2str(i)], [0 100]);
end
B1.SetParamRanges({'throttle_dt0','throttle_dt1', 'brake_dt0' }, [0 25]);
B1.SetParamRanges('brake_u0', [0 325]);
B1.SetParamRanges('brake_u1', [0 325]);

%% Input configuration 2
B2 = B0.copy(); 
B2.SetInputGen('UniStep20');
pu = B2.GetInputParamList();
pu_throttle = pu(~cellfun(@isempty, strfind(pu, 'throttle')));
pu_brake = pu(~cellfun(@isempty, strfind(pu, 'brake')));
B2.SetParamRanges(pu_brake, [0 325]);
B2.SetParamRanges(pu_throttle, [0 100]);


%% Requirement 
STL_ReadFile('requirements_breach.stl');
RAT1 = BreachRequirement(AT1);

%%  Corners Instance 2
B2co = B2.copy();

pb = FalsificationProblem(B2co, RAT1);
opt.group_by_inputs = true; 
pb.setup_corners('group_by_inputs', true);
pb.solve()