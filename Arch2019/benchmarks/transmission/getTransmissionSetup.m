function [B, allProps, allSimulationTimes] = getTransmissionSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
allProps{1} = STL_Formula('Problem1', 'alw_[0, 20] (speed[t]<120)');
allSimulationTimes(1) = 30;


%%%%%%%%%
% phi_2 %
%%%%%%%%%
allProps{2} = STL_Formula('Problem2', 'alw_[0, 10] RPM[t] < 4750');
allSimulationTimes(2) = 30;

%%%%%%%%%
% phi_3 %
%%%%%%%%%

% gear 1
allProps{3} = STL_Formula('Problem3', 'alw_[0.0, 30.0] (((not gear[t] == 1.0) and ev_[0.001, 0.1] (gear[t] == 1.0)) => ev_[0.001, 0.1] alw_[0.0, 2.5] (gear[t] == 1.0))');
allSimulationTimes(3) = 30;


%%%%%%%%%
% phi_4 %
%%%%%%%%%
% gear 2
allProps{4} = STL_Formula('Problem4', 'alw_[0.0, 30.0] (((not gear[t] == 2.0) and ev_[0.001, 0.1] (gear[t] == 2.0)) => ev_[0.001, 0.1] alw_[0.0, 2.5] (gear[t] == 2.0))');
allSimulationTimes(4) = 30;


%%%%%%%%%
% phi_5 %
%%%%%%%%%
% gear 3
allProps{5} = STL_Formula('Problem5', 'alw_[0.0, 30.0] (((not gear[t] == 3.0) and ev_[0.001, 0.1] (gear[t] == 3.0)) => ev_[0.001, 0.1] alw_[0.0, 2.5] (gear[t] == 3.0))');
allSimulationTimes(5) = 30;

%%%%%%%%%
% phi_6 %
%%%%%%%%%
% gear 4
allProps{6} = STL_Formula('Problem6', 'alw_[0.0, 30.0] (((not gear[t] == 4.0) and ev_[0.001, 0.1] (gear[t] == 4.0)) => ev_[0.001, 0.1] alw_[0.0, 2.5] (gear[t] == 4.0))');
allSimulationTimes(6) = 30;


%%%%%%%%%
% phi_7 %
%%%%%%%%%
% speed < 35
allProps{7} = STL_Formula('Problem12', '(alw_[0.0, 30.0] (RPM[t] < 3000.0) => alw_[0.0, 4.0] (speed[t] < 35.0))');
allSimulationTimes(7) = 30;

%%%%%%%%%
% phi_8 %
%%%%%%%%%
% speed < 50
allProps{8} = STL_Formula('Problem12', '(alw_[0.0, 30.0] (RPM[t] < 3000.0) => alw_[0.0, 8.0] (speed[t] < 50.0))');
allSimulationTimes(8) = 30;

%%%%%%%%%
% phi_9 %
%%%%%%%%%
% speed < 65
allProps{9} = STL_Formula('Problem12', '(alw_[0.0, 30.0] (RPM[t] < 3000.0) => alw_[0.0, 20.0] (speed[t] < 65.0))');
allSimulationTimes(9) = 30;


% Create the BreachSimulinkSystem object
B = BreachSimulinkSystem('Autotrans_shift');

% Number of control points: 7 for the accelerator, 3 for the brake
sig_gen = var_cp_signal_gen({'throttle', 'brake'}, [3 2], 'previous'); 
B.SetInputGen(sig_gen);

for i = 0:2
    B.SetParamRanges(['throttle_u' num2str(i)], [0 100]);
end

B.SetParamRanges({'throttle_dt0','throttle_dt1', 'brake_dt0' }, [0 25]);
B.SetParamRanges('brake_u0', [0 325]);
B.SetParamRanges('brake_u1', [0 325]);



%% Note: another input can be considered:
%% Input configuration 2 
% B.SetInputGen('UniStep20');
% pu = B.GetInputParamList();
% pu_throttle = pu(~cellfun(@isempty, strfind(pu, 'throttle')));
% pu_brake = pu(~cellfun(@isempty, strfind(pu, 'brake')));
% B.SetParamRanges(pu_brake, [0 325]);
% B.SetParamRanges(pu_throttle, [0 100]);



end

