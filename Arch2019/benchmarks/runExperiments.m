% RUNEXPERIMENTS Run all experiments from the paper
%   This script runs falsification for all models and specifications with
%   the same settings as in the paper. This means that we run 20
%   falsifications for each solver/param/semantics scenario, and that each
%   falsification runs a maximum of 1000 simulations. 

% IMPORTANT NOTE: For a standard computer, this will take several weeks to
% run. 

% To verify that all tests are runnable, run 'TEST_runExperiments.m' first.
% This only takes a couple of minutes and ensures that all models and specs
% work, and that the results can be written to both .txt and latex tables
% (.tex). 

% Set number of falsifications to 20, as in the paper
% This can be lowered for faster results, although they will not be
% equivalent to the ones in the paper. 
nFalsifications = 50;

% Allow a maximum of 1000 simulations (objective function evaluations) for 
% each falsification. 
% This can be lowered for faster results, although they will not be
% equivalent to the ones in the paper. 
maxEval = 300;

% Use all solvers
% One or more solvers can be removed for faster results. 
solversToUse = {'simulated_annealing', 'snobfit', 'cmaes', 'global_nelder_mead', 'uniform_random'};
%solversToUse = {'simulated_annealing'};

% Use all objective functions
% One or more objective functions can be removed for faster results.
% 'standard' here is 'max' in the paper
% 'vbool' here is 'additive' in the paper
% 'constant' here is 'constant' in the paper
objFunctionsToUse = {'standard', 'vbool', 'constant', 'MARV'};
%objFunctionsToUse = {'standard'};

% Index of props to use for chasing-cars benchmark
ChasingCars_propsToUse = 1:5;

% Index of props to use for F16 benchmark
F16_propsToUse = 1;

% Index of props to use for Neural benchmark
Neural_propsToUse = 1:2;

% Index of props to use for Powertrain benchmark
PowerTrain_propsToUse = 1:2;

% Index of props to use for Steam Condenser benchmark
SteamCondenser_propsToUse = 1;

% Index of props to use for Transmission benchmark
Transmission_propsToUse = 1:8;

% Index of props to use for Wind-Turbin benchmark
WindTurbine_propsToUse = 1:4;

% Run chasing-cars falsification
oneArchFalsification('chasing-cars', 'getChasingCarsSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, ChasingCars_propsToUse)

% Run F16 falsification
oneArchFalsification('F16_GCAS', 'getF16Setup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, F16_propsToUse)

% Run neural falsification
oneArchFalsification('neural', 'getNeuralSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, Neural_propsToUse)

% Run powertrain falsification
oneArchFalsification('powertrain', 'getPowertrainSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, PowerTrain_propsToUse)

% Run Steam Condenser falsification
oneArchFalsification('SteamCondenser', 'getSteamCondenserSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, SteamCondenser_propsToUse)

% Run transmission falsification
oneArchFalsification('transmission', 'getTransmissionSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, Transmission_propsToUse)

% Run wind turbine falsification
cd('wind-turbine');
init_breach_instances();
cd('..');
oneArchFalsification('wind-turbine', 'getWindTurbineSetup', nFalsifications, maxEval, solversToUse, objFunctionsToUse, WindTurbine_propsToUse)




% The variables will be stored in the 'mat' folders inside
% autotrans/modulator/switched folders. We load the latest .mat files in
% those folders (they are created with an ID that is the time and date of 
% the experiment, so the last file will be most up-to-date).  
% folders = {'autotrans', 'modulator', 'switched'};
% for folderCounter = 1:numel(folders)
%     thisFolder = folders{folderCounter};
%     fileList = dir([thisFolder '/mat']);
%     matFile = fileList(end).name;
%     
%     % Load all the variables in the .mat file
%     load([thisFolder '/mat/' matFile]);
% end
% 
% % Now we have all variables loaded in the current workspace. Some examples 
% % of how variables are named:
% % - autotrans_simulated_annealing_prop1
% % - modulator_global_nelder_mead_prop3
% % - switched_snobfit_prop2
% 
% % First, we print the variables to a text file. This is done in the script
% % 'printAll' as long as we have all variables loaded in this workspace. 
% % These results are saved in allResults.txt
% printAll;
% 
% % Then, we print the variables to Latex tables. This is done in the script
% % printAllToLatex as long as we have all variables loaded in this workspace
% % The tables are saved as .tex files in the latexFiles folder. 
% printAllToLatex;
% 
% % Finally, we create the cactus plot of all data. This corresponds to
% % Figure 2 in the paper. All variables need to be loaded in the workspace
% % to plot the correct cactus plots. 
% % The cactus plot is saved as a .tex file (tikz) and as a .png file in the
% % latexFiles folder. 
% createCactusPlot;
% 
% % Create the final latex file, which includes all the tables and figure in
% % the file main.tex which is put inside the latexFiles folder. 
% createFinalLatexFile;
