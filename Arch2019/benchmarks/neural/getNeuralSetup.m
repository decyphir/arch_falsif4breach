function [B, allProps, allSimulationTimes] = getNeuralSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 


% Initialization
assignin('base', 'u_ts', 0.001);

% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
% alph=0.005, bet=0.03

allProps{1} = STL_Formula('Problem1', 'alw_[1, 37] ((not (abs(Pos[t] - Ref[t]) < 0.005 + 0.03*abs(Ref[t]))) => ev_[0, 2] (alw_[0, 1] (abs(Pos[t] - Ref[t]) < 0.005 + 0.04*abs(Ref[t]))))');
% string1 = 'abs(Pos[t] - Ref[t]) < 0.005 + 0.03 * abs(Ref[t])';
% string2 = ['(ev_[0, 2] (alw_[0, 1](' string1 '))'];
% string3 = ['( not (' string1 '))'];
% allProps{1} = STL_Formula('Proble1', ['alw_[1, 37](' string3 ' => ' string2 ')']);
allSimulationTimes(1) = 40;



%%%%%%%%%
% phi_2 %
%%%%%%%%%
% alph=0.005, bet=0.04


allProps{2} = STL_Formula('Problem2', 'alw_[1, 37] ((not (abs(Pos[t] - Ref[t]) < 0.005 + 0.04*abs(Ref[t]))) => ev_[0, 2] (alw_[0, 1] (abs(Pos[t] - Ref[t]) < 0.005 + 0.04*abs(Ref[t]))))');
% string1 = 'abs(Pos[t] - Ref[t]) < 0.005 + 0.04 * abs(Ref[t])';
% string2 = ['(ev_[0, 2] (alw_[0, 1](' string1 '))'];
% string3 = ['( not (' string1 '))'];
% allProps{2} = STL_Formula('Problem3', ['alw_[1, 37](' string3 ' => ' string2 ')']);
allSimulationTimes(2) = 40;



% Create the BreachSimulinkSystem object
B = BreachSimulinkSystem('narmamaglev_v1');

% Setup input;
B.SetInputGen('UniStep3');
InputParams = B.GetInputParamList();
B.SetParamRanges(InputParams, [1 3]);

%% Note: It can be done with UniStep12

end