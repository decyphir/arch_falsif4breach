function [B, allProps, allSimulationTimes] = getF16Setup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialization:
addpath(genpath('AeroBenchVV-master'));
warning('off', 'f16:no_analysis');


% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
allProps{1} = STL_Formula('Problem1', 'alw (altitude[t]>0)');
allSimulationTimes(1) = 15;


% Set the input generator
sg = f16_signal_gen();
B = BreachSignalGen(sg);
B.SetTime(0:.01:15);

B.SetParamRanges( {'phig', 'thetag', 'psig'}, [ pi/4-pi/20 pi/4+pi/30;...
    -0.8*pi/2, -pi/2*0.8+pi/20; ...
    -pi/4-pi/8 -pi/4+pi/8]);


end

