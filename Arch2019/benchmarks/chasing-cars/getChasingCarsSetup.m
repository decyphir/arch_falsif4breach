function [B, allProps, allSimulationTimes] = getChasingCarsSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialization:
addpath(genpath('AeroBenchVV-master'));
warning('off', 'f16:no_analysis');


% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
allProps{1} = STL_Formula('Problem1', 'alw (Out5[t]-Out4[t]<=40)');
allSimulationTimes(1) = 15;


%%%%%%%%%
% phi_2 %
%%%%%%%%%
allProps{2} = STL_Formula('Problem2', 'alw_[0,70] ev_[0,30] (Out5[t]-Out4[t]>=15)');
allSimulationTimes(2) = 15;


%%%%%%%%%
% phi_3 %
%%%%%%%%%
allProps{3} = STL_Formula('Problem3', 'alw_[0,80] ((alw_[0,20] Out2[t]-Out1[t]<=20) or (ev_[0,20] Out5[t]-Out4[t]>=40))');
allSimulationTimes(3) = 15;


%%%%%%%%%
% phi_4 %
%%%%%%%%%
allProps{4} = STL_Formula('Problem4', 'alw_[0,65] ev_[0,30] alw_[0,5] (Out5[t]-Out4[t]>=8)');
allSimulationTimes(4) = 15;


%%%%%%%%%
% phi_5 %
%%%%%%%%%
allProps{5} = STL_Formula('Problem5', 'alw_[0,72] ev_[0,8] (alw_[0,5] (Out2[t]-Out1[t]>=9) => alw_[5,20] (Out5[t]-Out4[t]>= 9))');
allSimulationTimes(5) = 15;%%%%%%%%%



B = BreachSimulinkSystem('cars');

% Set the input generator
B.SetInputGen('UniStep4');
Inputs = B.GetInputParamList();
B.SetParamRanges(Inputs, [0 1.]);


% Note; This example cann be tested by Unistep20.

end

