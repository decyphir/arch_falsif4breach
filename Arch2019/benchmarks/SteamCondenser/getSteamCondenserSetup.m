function [B, allProps, allSimulationTimes] = getSteamCondenserSetup()
% GETATSYSTEMANDSPECS Get AT benchmark system and specifications
%   This function creates a BreachSimulinkSystem and also a cell array with
%   specifications to falsify for the Automatic Transmission benchmark
%   model. 
%
%   B is the BreachSimulinkSystem object that is used by Breach to simulate
%       the AT system. The model that is being simulated is
%       'autotrans_mod4.mdl'. 
%   allProps is a cell array with STL_Formula objects that will be used
%       for falsification. Note that there are different entries in
%       allProps when the same specification has two different parameter
%       values. 
%   allSimulationTimes is a vector that specifies which simulation end
%       time to use for each specification in propsToUse. 

% Initialize variables
allProps = {};
allSimulationTimes = [];

%%%%%%%%%
% phi_1 %
%%%%%%%%%
allProps{1} = STL_Formula('Problem1', 'alw_[30,35] (pressure[t] > 87 and pressure[t] < 87.5)');
allSimulationTimes(1) = 40;


B = BreachSimulinkSystem('steamcondense_RNN_22_BR');

B.SetInputGen('UniStep12');
p = B.GetInputParamList();
B.SetParamRanges(p, [3.99 4.01]);


end

